# 实验3：创建分区表

## 实验目的

掌握分区表的创建方法，掌握各种分区方式的使用场景。

## 实验内容

- 本实验使用实验2的sale用户创建两张表：订单表(orders)与订单详表(order_details)。
- 两个表通过列order_id建立主外键关联。给表orders.customer_name增加B_Tree索引。
- 新建两个序列，分别设置orders.order_id和order_details.id，插入数据的时候，不需要手工设置这两个ID值。
- orders表按订单日期（order_date）设置范围分区。
- order_details表设置引用分区。
- 表创建成功后，插入数据，数据应该能并平均分布到各个分区。orders表的数据都大于40万行，order_details表的数据大于200万行（每个订单对应5个order_details）。
- 写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划。
- 进行分区与不分区的对比实验。

## 实验步骤

- 创建订单表（orders）与订单详表（order_details）

  ![image-20230506230638377](test3.assets/image-20230506230638377.png)

  

- 创建索引和序列，给orders表的customer_nam列添加一个B-Tree索引；并orders.order_id和order_details.id，设置为自增。

  **![image-20230507113624989](test3.assets/image-20230507113624989.png)**

  

  

- 创建分区表，为 `orders` 表和 `order_details` 表分别创建分区表。

  - `orders`表按订单日期范围分区，为orders表创建按订单日期范围的分区表，分区键为order_date，按月分区，分区名格式为`p_yyyymm`：

    ![image-20230506231807316](test3.assets/image-20230506231807316.png)

    

  - `order_details`表引用分区，为`order_details`表创建引用分区，参考orders表的分区，分区键为`order_id`：

    ![image-20230506231846732](test3.assets/image-20230506231846732.png)

    

- 插入数据

  orders：

  ![image-20230507113858272](test3.assets/image-20230507113858272.png)

  order_detail

  ```sql
  INSERT INTO order_details_ref_partitioned (id, order_id, product_name, product_price, quantity)
  SELECT sale.seq_order_details_id.NEXTVAL,
         order_id,
         'product' || TO_CHAR(LEVEL, 'FM000000'),
         TRUNC(DBMS_RANDOM.VALUE(10, 100), 2),
         TRUNC(DBMS_RANDOM.VALUE(1, 10))
  FROM orders_range_partitioned
  WHERE order_id <= 400000
  CONNECT BY LEVEL <= 5;
  ```

  ![image-20230507120003537](test3.assets/image-20230507120003537.png)

- 执行计划

  ![img](test3.assets/_VA7Y%5B%7D5AOGTOISCIDIPLXX.png)

- 分区与不分区对比

  - 创建未分区orders与order_details

  - ```sql
    CREATE TABLE orders (
      order_id       NUMBER PRIMARY KEY,
      customer_name  VARCHAR2(100),
      order_date     DATE
    );
    ```

    ```sql
    CREATE TABLE order_details (
      id            NUMBER PRIMARY KEY,
      order_id      NUMBER,
      product_name  VARCHAR2(100),
      price         NUMBER,
      quantity      NUMBER,
      CONSTRAINT fk_order_details_orders FOREIGN KEY (order_id) REFERENCES orders(order_id)
    );
    ```

    

  - 创建未分区的索引

    ```
    CREATE INDEX idx_orders_customer_name ON orders(customer_name);
    ```

    ```
    CREATE SEQUENCE orders_seq;
    CREATE SEQUENCE order_details_seq;
    ```

    

  - 执行插入与查询比较时间

    ```
    -- 再执行同样的插入和查询语句进行对比
    
    -- 插入orders数据
    INSERT INTO orders (order_id, customer_name, order_date, total_amount)
    SELECT sale.seq_order_id.NEXTVAL,
           'customer' || TO_CHAR(LEVEL, 'FM000000'),
           TO_DATE('2021-01-01', 'YYYY-MM-DD') + TRUNC(DBMS_RANDOM.VALUE(0, 365*3)),
           TRUNC(DBMS_RANDOM.VALUE(100, 10000), 2)
    FROM DUAL
    CONNECT BY LEVEL <= 400000;
    
    -- 插入order_details数据
    INSERT INTO order_details (id, order_id, product_name, product_price, quantity)
    SELECT sale.seq_order_details_id.NEXTVAL,
           order_id,
           'product' || TO_CHAR(LEVEL, 'FM000000'),
           TRUNC(DBMS_RANDOM.VALUE(10, 100), 2),
           TRUNC(DBMS_RANDOM.VALUE(1, 10))
    FROM orders_range_partitioned
    WHERE order_id <= 400000
    CONNECT BY LEVEL <= 5;
    ```

    ## 实验总结

    在这个实验中，我们学习了Oracle数据库中表的分区技术。通过创建分区表、分区键和分区策略，可以将大型表分解成更小的子集，提高查询和管理数据的效率。我们还学习了如何在分区表中插入数据，如何查看分区表的信息以及如何管理分区表。这些技术都是非常实用的，可以在大型数据库应用中大大提高性能和可维护性。

    需要注意的是，分区表需要额外的存储空间，因此需要对数据库进行充分的规划和配置。同时，在创建和管理分区表时，也需要注意一些细节问题，如分区键的选择、分区策略的制定等等。因此，使用分区表技术需要有一定的经验和技能。

    总之，掌握Oracle分区表技术对于提高数据库应用的性能和可维护性非常重要。希望本次实验能够帮助您深入理解分区表的原理和应用，为您今后的数据库开发和管理工作提供帮助。

