<!-- markdownlint-disable MD033-->
<!-- 禁止MD033类型的警告 https://www.npmjs.com/package/markdownlint -->

# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 | [返回](./README.md)

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|

## 方案及其内容

1. 设计表空间

   ```
   CREATE TABLESPACE DATA_TS DATAFILE '/home/oracle/app/oracle/oradata/orcl/pdborcl/data_ts.dbf' SIZE 100M;
   
   CREATE TABLESPACE INDEX_TS DATAFILE '/home/oracle/app/oracle/oradata/orcl/pdborcl/index_ts.dbf' SIZE 50M;
   
   ```

   ![image-20230525220002356](test6.assets/image-20230525220002356.png)

   首先为表分配空间结构，便于后期表格的创建

   

2. 创建数据表

   ```sql
   -- 创建顾客表
   CREATE TABLE customers (
     customer_id NUMBER PRIMARY KEY,
     name VARCHAR2(50),
     address VARCHAR2(100),
     contact VARCHAR2(50)
   ) TABLESPACE DATA_TS;
   
   -- 创建商品表
   CREATE TABLE products (
     product_id NUMBER PRIMARY KEY,
     name VARCHAR2(50),
     description VARCHAR2(100),
     price NUMBER
   ) TABLESPACE DATA_TS;
   
   -- 创建订单表
   CREATE TABLE orders (
     order_id NUMBER PRIMARY KEY,
     customer_id NUMBER,
     order_date DATE,
     total_amount NUMBER,
     FOREIGN KEY (customer_id) REFERENCES customers(customer_id)
   ) TABLESPACE DATA_TS;
   
   -- 创建订单详情表
   CREATE TABLE order_details (
     order_id NUMBER,
     product_id NUMBER,
     quantity NUMBER,
     price NUMBER,
     PRIMARY KEY (order_id, product_id),
     FOREIGN KEY (order_id) REFERENCES orders(order_id),
     FOREIGN KEY (product_id) REFERENCES products(product_id)
   ) TABLESPACE DATA_TS;
   ```

   设计了顾客表，包含（顾客id，顾客name，address，contact），商品表，包含（商品id，商品name，商品描述，以及价格），订单表，包含（订单id，顾客id，订单时间，订单总金额），订单详情表，包含（订单id，商品id，商品数量，商品价格，并且将订单id索引到orders下的订单id，商品id索引到products下的商品id）

   ![image-20230525220111756](test6.assets/image-20230525220111756.png)

   

3. 设计权限及用户分配方案

   ```
   -- 创建用户
   CREATE USER sale_user IDENTIFIED BY password DEFAULT TABLESPACE DATA_TS;
   CREATE USER admin_user IDENTIFIED BY password DEFAULT TABLESPACE DATA_TS;
   
   ```

   ![image-20230525220201617](test6.assets/image-20230525220201617.png)

   完成创建两个用户，一个是实际顾客，一个是备份用户，并为其赋予权限

   ```
   
   -- 赋予sale_user表和程序包的访问权限
   GRANT SELECT, INSERT, UPDATE, DELETE ON customers TO sale_user;
   GRANT SELECT, INSERT, UPDATE, DELETE ON products TO sale_user;
   GRANT SELECT, INSERT, UPDATE, DELETE ON orders TO sale_user;
   GRANT SELECT, INSERT, UPDATE, DELETE ON order_details TO sale_user;
   GRANT EXECUTE ON sale_pkg TO sale_user;
   
   -- 赋予admin_user完整的数据库备份和恢复权限
   GRANT SYSDBA TO admin_user;
   
   ```

   ![image-20230525221055521](test6.assets/image-20230525221055521.png)

   ​	![image-20230525221135244](test6.assets/image-20230525221135244.png)

   应当先创建程序包，才能给sale_user赋予相应的权限

   

4. 创建程序包和存储过程

   1. 创建程序包

      ```
      sqlCopy code-- 创建程序包
      CREATE OR REPLACE PACKAGE sale_pkg AS
        -- 存储过程：创建订单
        PROCEDURE create_order(
          p_customer_id IN NUMBER,
          p_order_date IN DATE,
          p_product_id IN NUMBER,
          p_quantity IN NUMBER
        );
      END sale_pkg;
      /
      ```

      ![image-20230525221039022](test6.assets/image-20230525221039022.png)

   2. 创建存储过程

   ```
   CREATE OR REPLACE PACKAGE BODY sale_pkg AS
     PROCEDURE create_order(
       p_customer_id IN NUMBER,
       p_order_date IN DATE,
       p_product_id IN NUMBER,
       p_quantity IN NUMBER
     ) IS
       v_order_id NUMBER;
       v_total_amount NUMBER;
     BEGIN
       -- 插入订单记录
       INSERT INTO orders (order_id, customer_id, order_date, total_amount)
       VALUES (order_seq.NEXTVAL, p_customer_id, p_order_date, NULL)
       RETURNING order_id INTO v_order_id;
   
       -- 插入订单详情记录
       INSERT INTO order_details (order_id, product_id, quantity, price)
       VALUES (v_order_id, p_product_id, p_quantity, NULL);
   
       -- 更新订单总金额
       SELECT SUM(quantity * price) INTO v_total_amount
       FROM order_details
       WHERE order_id = v_order_id;
   
       UPDATE orders
       SET total_amount = v_total_amount
       WHERE order_id = v_order_id;
     END create_order;
   END sale_pkg;
   
   ```

   上面内容实现了三种方法，分别是插入订单，插入订单详情记录，和更新订单总金额

   

   
