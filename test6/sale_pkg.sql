CREATE OR REPLACE PACKAGE BODY sale_pkg AS
  PROCEDURE create_order(
    p_customer_id IN NUMBER,
    p_order_date IN DATE,
    p_product_id IN NUMBER,
    p_quantity IN NUMBER
  ) IS
    v_order_id NUMBER;
    v_total_amount NUMBER;
  BEGIN
    -- 插入订单记录
    INSERT INTO orders (order_id, customer_id, order_date, total_amount)
    VALUES (order_seq.NEXTVAL, p_customer_id, p_order_date, NULL)
    RETURNING order_id INTO v_order_id;

    -- 插入订单详情记录
    INSERT INTO order_details (order_id, product_id, quantity, price)
    VALUES (v_order_id, p_product_id, p_quantity, NULL);

    -- 更新订单总金额
    SELECT SUM(quantity * price) INTO v_total_amount
    FROM order_details
    WHERE order_id = v_order_id;

    UPDATE orders
    SET total_amount = v_total_amount
    WHERE order_id = v_order_id;
  END create_order;
END sale_pkg;
